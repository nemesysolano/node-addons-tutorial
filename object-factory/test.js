// test.js
const factory = require('./build/Release/object_factory');

const obj1 = factory('hello');
const obj2 = factory('world');
console.log(obj1.msg, obj2.msg);
// Prints: 'hello world'